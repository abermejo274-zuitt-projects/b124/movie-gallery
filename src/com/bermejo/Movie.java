package com.bermejo;

public class Movie {
    private String name;
    private String category;

    public Movie(){}

    public Movie(String newName, String newCategory) {
        this.name = newName;
        this.category = newCategory;
    }

    public String getName() {
        return this.name;
    }
    public String getCategory() {
        return this.category;
    }

    public void setName(String newName) {
        this.name = newName;
    }
    public void setCategory(String newCategory) {
        this.category = newCategory;
    }

    public String display() {
        return "Name: " + this.getName() + "  " + "Category: " + this.getCategory();
    }

}

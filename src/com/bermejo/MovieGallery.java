package com.bermejo;

import java.util.ArrayList;
import java.util.Scanner;

public class MovieGallery {
    private boolean willExit;
    ArrayList<Movie> movieList = new ArrayList<>();

    public MovieGallery(){}

    public void runMovieGallery(){
        printHeader();
        runMenu();
    }

    private void printHeader() {
        System.out.println("+------------------------------+");
        System.out.println("|          Welcome to          |");
        System.out.println("|        Movie Gallery         |");
        System.out.println("+------------------------------+");
    }

    private void runMenu() {
        while(!willExit) {
            printMenu();
            int select = getInput();
            performAction(select);
        }
    }

    private void printMenu() {
        System.out.println("\n+------------------------+");
        System.out.println("Please make a selection:");
        System.out.println("1) Add movie ");
        System.out.println("2) Browse all movies");
        System.out.println("0) Exit");
        System.out.println("+------------------------+");
    }

    private int getInput() {
        Scanner input = new Scanner(System.in);
        int select = -1;
        while(select < 0 || select > 2){
            try {
                System.out.println("Enter the number of your selection: ");
                select = Integer.parseInt(input.nextLine());
            }
            catch(NumberFormatException e) {
                System.out.println("Invalid selection. Please try again.");
            }
        }
        return select;
    }

    private void performAction(int select) {
        switch(select){
            case 0:
                willExit = true;
                System.out.println("\n***** Output *****");
                System.out.println("Thank you for using Movie Gallery application.");
                break;
            case 1:
                addMovie();
                break;
            case 2:
                displayMovies();
                break;
        }
    }

    private void addMovie(){
        Scanner nameInput = new Scanner(System.in);
        Scanner categoryInput = new Scanner(System.in);

        System.out.println("Enter the movie title:");
        String name = nameInput.nextLine();
        System.out.println("Enter the movie category:");
        String category = categoryInput.nextLine();

        Movie newMovie = new Movie(name, category);
        movieList.add(newMovie);

        System.out.println("\n***** Output *****");
        System.out.println("The entry was added to the collection of movies.");
    }

    private void displayMovies(){
        if(movieList.size() == 0) {
            System.out.println("\n***** Output *****");
            System.out.println("No available movies yet.");
        } else {
            System.out.println("\n***** Output *****");
            System.out.println("Available movies:");
            for (int i = 0; i < movieList.size(); i++) {
                System.out.println((i+1) + ") " + movieList.get(i).display());
            }
        }
    }

}
